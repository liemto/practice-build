pipeline {
    parameters {
        string(name: 'VERSION_STRING', defaultValue: '', description: 'Release version of project.')
        booleanParam(name: 'JENKINS_BUILD_SECURE_IMAGE', defaultValue: false, description: 'Generate secure upgrade package for each build type (engineering and/or production).')
        persistentBoolean(name: 'USE_BETA_SPLASH', defaultValue: false, description: 'Persistent parameter - Use Beta Splash Screen', successfulOnly: false)
        choice(name: 'MANIFEST_REPO', choices: ['red-manifest.git', 'red-one-off-manifest.git'], description: 'Manifest git repository, from ssh://git@bitbucket.org/reddigitalcinema/')
        choice(name: 'MANIFEST', choices: ['corona/corona-dev.xml', 'corona/branches/corona-1.2.xml'])
        string(name: 'MANIFEST_BRANCH', defaultValue: 'master')
    }
    agent {
        label 'repo-docker'
    }
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out the code '
                echo "repo init -u ssh://git@bitbucket.org/reddigitalcinema/${params.MANIFEST_REPO} -b ${params.MANIFEST_BRANCH} -m ${params.MANIFEST}"
                sh "repo init -u ssh://git@bitbucket.org/reddigitalcinema/${params.MANIFEST_REPO} -b ${params.MANIFEST_BRANCH} -m ${params.MANIFEST}"
                sh "repo sync -d -j4"
                sh 'repo forall -p -c "git lfs pull"'
            }
        }
        stage('Build') {
            steps {
                echo 'Building the application... '
                sh '''
                    rm -f /tmp/.X11-unix/X*
                    export MAJOR_VERSION_NUMBER=0
                    export MINOR_VERSION_NUMBER=0
                    export PATCH_VERSION_NUMBER=0
                    export IS_DEV_BRANCH=1
                    export JENKINS_IMAGE_TYPE="engineering only"
                    cd corona-v2020.1
                    ./build.sh
                '''
            }
        }
        stage('Test') {
            steps {
                echo 'Testing the application... '
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying the application... '
            }
        }
    }
    post {
        success {
            archiveArtifacts artifacts: 'corona-v2020.1/images-engineering/linux/upgrade.bin'
        }
        failure {
            mail to: 'liem.to@red.com',
            subject: "Failed Pipeline: ${currentBuild.fullDisplayName}",
            body: "Something is wrong with ${env.BUILD_URL}"
        }
        cleanup {
            echo "Cleaning up after build..."
        }
    }
}